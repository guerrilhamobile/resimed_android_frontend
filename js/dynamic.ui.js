//pega a altura e largura do viewport
var viewportH = verge.viewportH();
var viewportW = verge.viewportW();

//seta uma referência para o stylesheet a ser utilizado no próprio documento
var styleTag = document.getElementById ("dynamicStyle");
var sheet = styleTag.sheet ? styleTag.sheet : styleTag.styleSheet;

//função que adiciona as tags html e seus atributos css dentro do style
function addCSSRule(sheet, selector, rules, index){
    if("insertRule" in sheet) {
        sheet.insertRule(selector + "{" + rules + "}", index);
    }
    else if("addRule" in sheet) {
        sheet.addRule(selector, rules, index);
    }
}

/*-------------- SPLASHSCREEN --------------*/
if(document.querySelector("#logoSplashScreen")){

    if(viewportW <= 360){
        var logoSplashWidth = viewportW - 120;
        addCSSRule(sheet, '#logoSplashScreen', 'width: '+logoSplashWidth+'px; margin-left: 60px;', 0);
        var logoSplashHeight = document.querySelector("#logoSplashScreen").height;
        var logoSplashMarginTop = (viewportH - logoSplashHeight) / 2
        addCSSRule(sheet, '#logoSplashScreen', 'margin-top: '+logoSplashMarginTop+'px;', 0);
    }else{
        //var logoSplashHeight = document.querySelector("#logoSplashScreen").height;
        var logoSplashMarginTop = (viewportH - 150) / 2;
        var logoSplashMarginLeft = (viewportW - 240) / 2;
        addCSSRule(sheet, '#logoSplashScreen', 'width: 240px; margin-left: '+logoSplashMarginLeft+'px; margin-top: '+logoSplashMarginTop+'px', 0);
    }

    window.addEventListener("resize", function() {

        //pega a altura e largura do viewport
        viewportH = verge.viewportH();
        viewportW = verge.viewportW();
        
        var element = document.querySelector("#logoSplashScreen");

        if(viewportW <= 360){
            var logoSplashWidth = viewportW - 120;
            element.style.width = logoSplashWidth+'px';
            element.style.marginLeft = '60px';

            var logoSplashHeight = document.querySelector("#logoSplashScreen").height;
            var logoSplashMarginTop = (viewportH - logoSplashHeight) / 2;

            element.style.marginTop = logoSplashMarginTop+'px';
        }else{
            var logoSplashMarginTop = (viewportH - 150) / 2;
            var logoSplashMarginLeft = (viewportW - 240) / 2;

            element.style.width = '240px';
            element.style.marginLeft = logoSplashMarginLeft+'px';
            element.style.marginTop = logoSplashMarginTop+'px';
        }

    }, false);
    
}

/*-------------- TELA INICIAL --------------*/
if(document.querySelector("#sem-edital")){

    var divSemEdital = document.querySelector("#sem-edital");
    var divReport = document.querySelector(".report");
    var aBuscaEditais = document.querySelector(".bt-busca-editais");


    var divSemEditalH = divSemEdital.clientHeight;
    var divSemEditalW = divSemEdital.clientWidth;

    divReport.style.marginTop = (divSemEditalH - 56 - divReport.clientHeight) / 2 + "px";

    aBuscaEditais.style.marginTop = (divSemEditalH - divReport.clientHeight);


}


